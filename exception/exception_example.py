

class contmanager():

    def __init__(self, filename):
        self.file_handler = None
        try:
            self.file_handler = open(filename,"w")
        except FileNotFoundError:
            print("No such file")


    def __enter__(self):
        print("started")

        return self.file_handler

    def __exit__(self, *err):

        if self.file_handler != None:
            self.file_handler.close()
            self.file_handler = None
            print("file closed")

        print("exit")
        return False

with contmanager("cm.log") as cm:
    print("inside")
    cm.write("line of text")

print("-----------")


from contextlib import contextmanager

@contextmanager
def easycontmanager(filename):
    file_handler = open(filename, 'w')
    try:
        yield file_handler
    finally:
        file_handler.close()

with easycontmanager("cm2.log") as f:
    f.write("another line of text")