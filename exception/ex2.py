import time
import random


def LoginDecorator(retries):
    def FuncDec(func):
        def handler():
            i = retries
            result = False
            while (not result and i > 0):
                i = i - 1
                result = func()        

            return result
        return handler
    return FuncDec

ret = 3
@LoginDecorator(ret)
def LoginFunc():
    i = random.randint(0,10) / 10
    time.sleep(i)
    print("Trying to connect")

    # cant conect to server
    return False

print(LoginFunc())
