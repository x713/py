import random
import time

# decorator logger
def logdec(filename, delta_time = 0.5):
    def dec(func):
        def handler():
            t_start = time.time()
            res = func()
            t_delta = time.time() - t_start

            if t_delta >= delta_time:
                with open(filename, "a") as f:
                    f.write(f"Function {func.__name__}() was executed in {t_delta} ms\n")
                
            return res
        return handler
    return dec

logger_filename = "perf.log"
# original func
@logdec(logger_filename, delta_time = 1)
def foo():
    i = random.randint(0,20) / 10
    time.sleep(i)
    return i

print("Executing function. Please wait\n")

for i in range(10):
    print(foo())

print("Logfile content:\n")
f = open(logger_filename)
for line in f.readlines():
    print(line)

print("Executing done")
