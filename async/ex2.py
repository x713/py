import asyncio
import time

async def say_after(delay, what):
    await asyncio.sleep(delay)
    print(what)

async def main():
    print(f"started at {time.strftime('%X')}")

    await say_after(1, "one")
    await say_after(2, "two")

    print(f"end at {time.strftime('%X')}")

asyncio.run(main())