def sleep_in(weekday, vacation):

  if not weekday or vacation:

    return True

  else:

    return False

def monkey_trouble(a_smile, b_smile):

  if (a_smile and b_smile) or (not a_smile and not b_smile):

    return True

  else:

    return False


def sum_double(a, b):

  if (a == b):

    return 2*(a+b)

  else:

    return a+b


def diff21(n):

  diff = abs(21-n)

  if (n > 21):

    return 2*diff

  else:

    return diff


def parrot_trouble(talking, hour):

  if talking and (hour  < 7 or hour > 20):

    return True

  else:

    return False


def makes10(a, b):

  if (a == 10 or b == 10 or a+b==10):

    return True

  else:

    return False

def near_hundred(n):

  if (abs(100-n)<=10) or (abs(200-n)<=10):

    return True

  else:

    return False

def pos_neg(a, b, negative):

  if not negative and a*b<0:

    return True

  elif negative and a<0 and b<0:

    return True

  else:

    return False


def not_string(str):

  if 'not' in str[0:4]:

    return str

  else:

    return 'not ' + str

def missing_char(str, n):

  return str[0:n] + str[n+1:]

def front_back(str):

  l = len(str)

  if l <= 1:

    return str

  elif l == 2:

    return str[1] + str[0]

  else:

    return str[l-1] + str[1:l-1] + str[0]

def front3(str):

  l = len(str)

  if l < 3:

    s = str + str +str

  else:

    s = str[:3]+str[:3]+str[:3]

  return s