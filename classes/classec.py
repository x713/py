

class some_class(object):

    def __init__(self, some_param):
        print(f"Init class with param = {some_param}")
        self.param = some_param
    
    def __getattribute__(self, name):
        print(f"Get attribute {name}")
        return super().__getattribute__(name)

    def __getattr__(self, name):
        print("Not found. Search again")
        # return None

    @staticmethod
    def some_static_method():
        print("No class instance, but method called")
        


a = some_class("par")

print(a.b)

print(a.param)

some_class.some_static_method()
a.some_static_method();